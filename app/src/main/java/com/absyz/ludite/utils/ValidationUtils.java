package com.absyz.ludite.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtils {

    private static Pattern pattern;
    private static Matcher matcher;
    private static String PASSWORD_REGEX = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$%^*()_]).{8,}$";

    //for windowmanager
    private Context context;

    public ValidationUtils(Context ctx) {
        this.context = ctx;
    }

    public ValidationUtils() {

    }

    /**
     * Email ID Validation
     * @param email
     * @return
     */
    public static boolean isValidEmailID(String email) {
        String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_REGEX);
        matcher = pattern.matcher(email);
        return !matcher.matches();
    }

    /**
     * Password Validation
     * @param password
     * @return
     */
    public static boolean isValidPsd(String password) {
        pattern = Pattern.compile(PASSWORD_REGEX);
        matcher = pattern.matcher(password);
        return !matcher.matches();
    }


    /**
     * Hide Soft Keypad
     * @param context
     * @param input
     */
    public static void hideSoftKeyboard(Context context, EditText input) {
        input.setInputType(0);
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
        }
    }


    /**
     * Hide Soft Keypad
     * @param activity
     */
    public static void hideKeyboard(final Activity activity) {
        try {
            final View view = activity.getCurrentFocus();
            if (view != null) {
                view.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // TODO Auto-generated method stub
                        InputMethodManager keyboard = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (keyboard != null) {
                            keyboard.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }
                }, 50);
            }
            if (activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void shapeDrawableWithOutAlpha(View v, int backgroundColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[]{10, 10, 10, 10, 10, 10, 10, 10});
        shape.setColor(backgroundColor);
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            v.setBackgroundDrawable(shape);
        } else {
            v.setBackground(shape);
        }
    }


    public static boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 9 || phone.length() > 13) {
                // if(phone.length() != 10)
                check = false;
                // txtPhone.setError("Not Valid Number");
            } else {
                check = android.util.Patterns.PHONE.matcher(phone).matches();
            }
        } else {
            check = false;
        }
        return check;
    }


}
