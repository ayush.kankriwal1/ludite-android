package com.absyz.ludite.inject.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

/**
 * Created by Ayush on 28/06/2020.
 */

public class CustomButton extends AppCompatButton {

    public CustomButton(Context context) {
        super(context);
        setFont(context);
    }

    public CustomButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public CustomButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }

    private void setFont(Context context) {
        Typeface normalTypeface = FontCache.getTypeface( constant.FONT_REGULAR, context);
        setTypeface(normalTypeface);
    }

}
