package com.absyz.ludite.inject.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by Ayush on 28/05/2020.
 */

public class CustomRobotoLight extends AppCompatTextView {

    public CustomRobotoLight(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public CustomRobotoLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public CustomRobotoLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface normalTypeface = FontCache.getTypeface( constant.FONT_LIGHT, context);
        this.setTypeface(normalTypeface);
        this.setLineSpacing(0.1f,1.0f);

    }
}