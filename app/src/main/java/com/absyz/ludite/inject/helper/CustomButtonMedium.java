package com.absyz.ludite.inject.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

public class CustomButtonMedium extends AppCompatButton {

    public CustomButtonMedium(Context context) {
        super(context);
        setFont(context);
    }

    public CustomButtonMedium(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public CustomButtonMedium(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }

    private void setFont(Context context) {
        Typeface normalTypeface = FontCache.getTypeface( constant.FONT_ROBOTO_MEDIUM, context);
        setTypeface(normalTypeface);
    }

}
