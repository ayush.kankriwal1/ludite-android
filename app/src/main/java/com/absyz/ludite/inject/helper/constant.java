package com.absyz.ludite.inject.helper;

public interface constant {
    String FONT_REGULAR = "font/robotoregular.ttf";
    String FONT_ROBOTO_MEDIUM = "font/robotomedium.ttf";
    String FONT_BOLD = "font/robotobold.ttf";
    String FONT_THIN = "font/robotolight.ttf";
    String FONT_LIGHT = "font/robotoregular.ttf";
}
