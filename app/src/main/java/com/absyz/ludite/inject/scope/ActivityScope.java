package com.absyz.ludite.inject.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Ayush on 26-05-2020.
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface ActivityScope {
}
