package com.absyz.ludite.inject.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.ItemRowDrawerBinding;
import com.absyz.ludite.ui.base.DrawerModel;

import java.util.ArrayList;

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {

    private Context context;
    private ArrayList<DrawerModel> modelArrayList;
//    private Utility utility = Utility.getInstance();
    private DrawerItemClick drawerItemClick;

    ItemRowDrawerBinding itemRowDrawerBinding;

    public DrawerAdapter(Context context, ArrayList<DrawerModel> modelArrayList, DrawerItemClick drawerItemClick) {
        this.context = context;
        this.modelArrayList = modelArrayList;
        this.drawerItemClick = drawerItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        itemRowDrawerBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_row_drawer, parent, false);

        return new ViewHolder(itemRowDrawerBinding);


    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            final DrawerModel drawerModel = modelArrayList.get(position);
            itemRowDrawerBinding.titleTV.setText(drawerModel.getTitle());
            itemRowDrawerBinding.titleImageV.setBackground(drawerModel.getDrawableInt());

            if (drawerModel.getItemID() == 9) {
                itemRowDrawerBinding.unReadCountTV.setVisibility(View.GONE);
                itemRowDrawerBinding.unReadCountTV.setText("" + drawerModel.getUnReadCount());
            } else {
                itemRowDrawerBinding.unReadCountTV.setVisibility(View.GONE);
                itemRowDrawerBinding.unReadCountTV.setText("0");
            }

            itemRowDrawerBinding.mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        drawerItemClick.onDrawerItemClick(drawerModel.getItemID());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return modelArrayList != null ? modelArrayList.size() : 0;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemRowDrawerBinding itemRowBinding;

        public ViewHolder(ItemRowDrawerBinding itemView) {
            super(itemView.getRoot());
            this.itemRowBinding = itemView;

        }
    }

    public interface DrawerItemClick {
        void onDrawerItemClick(int position);
    }

}