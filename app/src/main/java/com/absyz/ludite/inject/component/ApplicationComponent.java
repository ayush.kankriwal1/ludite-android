package com.absyz.ludite.inject.component;


import com.absyz.ludite.inject.ApplicationModule;
import com.absyz.ludite.inject.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Ayush on 26-05-2020.
 */

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {



}
