package com.absyz.ludite.inject.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

/**
 * Created by Ayush on 28/06/2020.
 */

public class CustomRobotoEditText extends AppCompatEditText {
    public CustomRobotoEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public CustomRobotoEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        if (!isInEditMode()) {
            Typeface normalTypeface = FontCache.getTypeface( constant.FONT_REGULAR, context);
            this.setTypeface(normalTypeface);
        }
    }
}
