package com.absyz.ludite.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.PasswordActivityBinding;
import com.absyz.ludite.utils.ValidationUtils;

import java.util.Objects;

public class Password_Activity extends AppCompatActivity {

    /*activity dataBinding*/
    PasswordActivityBinding PassowrdActivityBinding;
    private String strPassword;
    private boolean ischeckpassword = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PassowrdActivityBinding = DataBindingUtil.setContentView(Password_Activity.this, R.layout.password_activity);
        PassowrdActivityBinding.btNext.setAlpha(0.5f);
        PassowrdActivityBinding.btNext.setClickable(false);
        PassowrdActivityBinding.btNext.setEnabled(false);
        PassowrdActivityBinding.passwordshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strPassword = PassowrdActivityBinding.etPassword.getText().toString();
                if (!ischeckpassword && !TextUtils.isEmpty(strPassword)) {
                    //  passwordshow.setText("Hide");
                    PassowrdActivityBinding.etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    ischeckpassword = true;
                    PassowrdActivityBinding.etPassword.setSelection(strPassword.length());
                    PassowrdActivityBinding.passwordshow.setImageResource(R.drawable.ic_eye_hide);

                } else {

                    // passwordshow.setText("Show");
                    PassowrdActivityBinding.etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    ischeckpassword = false;
                    PassowrdActivityBinding.etPassword.setSelection(PassowrdActivityBinding.etPassword.length());
                    PassowrdActivityBinding.passwordshow.setImageResource(R.drawable.ic_eye_show);
                }
            }
        });

        PassowrdActivityBinding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                strPassword = Objects.requireNonNull(PassowrdActivityBinding.etPassword.getText()).toString();
                if (strPassword.isEmpty()) {
                    PassowrdActivityBinding.btNext.setAlpha(0.3f);
                    PassowrdActivityBinding.btNext.setClickable(false);
                    PassowrdActivityBinding.btNext.setEnabled(false);
                    PassowrdActivityBinding.errorPassword.setText("");

                } else {
                    PassowrdActivityBinding.btNext.setAlpha(1f);
                    PassowrdActivityBinding.btNext.setClickable(true);
                    PassowrdActivityBinding.btNext.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                PassowrdActivityBinding.errorPassword.setVisibility(View.INVISIBLE);

            }
        });
        PassowrdActivityBinding.btNext.setOnClickListener(v -> {
            passordValidation();
        });
    }


    private void passordValidation() {
        strPassword = PassowrdActivityBinding.errorPassword.getText().toString();

        if (strPassword.isEmpty()) {
            PassowrdActivityBinding.errorPassword.setVisibility(View.VISIBLE);
            PassowrdActivityBinding.errorPassword.setText(getString(R.string.enter_password));
        } else
            if (!ValidationUtils.isValidPsd(strPassword)) {
                PassowrdActivityBinding.errorPassword.setText(getResources().getString(R.string.enter_valid_password));
                PassowrdActivityBinding.errorPassword.setVisibility(View.VISIBLE);

        } else {
            startActivity(new Intent(Password_Activity.this, AddDateofBirth_Activity.class));
        }
    }
}