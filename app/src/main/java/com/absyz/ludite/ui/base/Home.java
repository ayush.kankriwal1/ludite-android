package com.absyz.ludite.ui.base;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.HomeBinding;
import com.absyz.ludite.inject.adapter.DrawerAdapter;
import com.absyz.ludite.inject.helper.DividerItemDecoration;
import com.absyz.ludite.ui.MomentsCaptureActivity;
import com.absyz.ludite.ui.fragments.HomeFragment;
import com.absyz.ludite.ui.fragments.MomentsFragment;
import com.absyz.ludite.ui.fragments.NotificationsFragment;
import com.absyz.ludite.ui.fragments.RankingFragments;
import com.absyz.ludite.ui.fragments.SearchFragment;
import com.absyz.ludite.ui.fragments.SettingsFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class Home extends AppCompatActivity implements  DrawerAdapter.DrawerItemClick, TabLayout.BaseOnTabSelectedListener {
    HomeBinding homeBinding;
    private ActionBarDrawerToggle mDrawerToggle;
    DrawerAdapter drawerAdapter;
    private static final String TAG = "Home";
    private int currentSelected = 0;
    AppCompatImageView ImView; ;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeBinding = DataBindingUtil.setContentView(Home.this, R.layout.home);

        setDrawerToggleAction();
        setAdapterSliderMenu();
        setupTabIcons();
        implementListeners();


        homeBinding.toolbarhome.toolbarHomeImage.setOnClickListener(v -> {
            startActivity(new Intent(this, MomentsCaptureActivity.class));
        });

    }

    /**
     * method to add tabs items
     */
    private void setupTabIcons() {
        setCustomLayout(getResources().getDrawable(R.drawable.ic_inner_bluefriends));
        setCustomLayout(getResources().getDrawable(R.drawable.ic_innner_circle_blue));
        setCustomLayout(getResources().getDrawable(R.drawable.ic_memories));
    }

    /**
     * method to set custom view for tabs items
     */
    private void setCustomLayout(Drawable drawable) {
        ConstraintLayout customTab = (ConstraintLayout) LayoutInflater.from(this)
                .inflate(R.layout.custom_item_tab_home, null);
        ImView = customTab.findViewById(R.id.tv_icon);
        ImView.setBackgroundDrawable(drawable);
        homeBinding.toolbarhome.homebutton.tabLayout.addTab(homeBinding.toolbarhome.homebutton.tabLayout.newTab().setCustomView(customTab));

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setAdapterSliderMenu() {
        try {
            ArrayList<DrawerModel> drawerModels = getDrawersMenuItems();
            drawerAdapter = new DrawerAdapter(Home.this, drawerModels, Home.this);
            homeBinding.recyclerViewMenu.recyclerView.setLayoutManager(new LinearLayoutManager(Home.this));
            //recyclerViewMenu.addItemDecoration(new DividerItemDecoration(HomeActivity.this, LinearLayoutManager.VERTICAL, 20, 1));
            homeBinding.recyclerViewMenu.recyclerView.addItemDecoration(new DividerItemDecoration(Home.this));
            homeBinding.recyclerViewMenu.recyclerView.setAdapter(drawerAdapter);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



    private void setDrawerToggleAction() {
        try {

            setSupportActionBar(homeBinding.toolbarhome.toolbar);

            mDrawerToggle = new ActionBarDrawerToggle(this, homeBinding.drawerLayout, homeBinding.toolbarhome.toolbar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
                public void onDrawerClosed(View view) {
                    invalidateOptionsMenu();
                }

                public void onDrawerOpened(View drawerView) {
                    invalidateOptionsMenu();
                }
            };
            mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openCloseDrawer();
                    //loadFragment(new HomeFragment(), "");

                }
            });

            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

            homeBinding.drawerLayout.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();
            mDrawerToggle.setDrawerIndicatorEnabled(false);//if set true it will show default hamburger icon with spinner action when opne/close.
            mDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_hamburger_menu);
            mDrawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorPrimary));// will change hamburger icon color


//            hamburgerVisibility(true);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    private void openCloseDrawer() {
        if (homeBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            homeBinding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            homeBinding.drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onDrawerItemClick(int position) {
        try {
            Log.i(TAG, "onDrawerItemClick - " + position);
            switch (position) {
                case 0:
                    openCloseDrawer();
                    replaceFragment(new HomeFragment());

                    //onChangesLanguage();
                    break;
                case 1:
                    openCloseDrawer();
                    replaceFragment(new SearchFragment());
                    break;
                case 2:
                    openCloseDrawer();
                    replaceFragment(new SettingsFragment());

                    break;
                case 3:
                    openCloseDrawer();
                    replaceFragment(new NotificationsFragment());
                    break;
                case 4:
                    openCloseDrawer();
                    replaceFragment(new RankingFragments());
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Method to replace fragments
     *
     * @param fragment fragment to be replaced
     */
    public void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private ArrayList<DrawerModel> getDrawersMenuItems() {
        ArrayList<DrawerModel> drawerModels = new ArrayList();
        DrawerModel drawerModel;
        try {

            drawerModel = new DrawerModel();
            drawerModel.setTitle(getString(R.string.home_text));
            drawerModel.setItemID(10);
            drawerModel.setDrawableInt(getDrawable(R.drawable.ic_home));
            drawerModels.add(drawerModel);

            drawerModel = new DrawerModel();
            drawerModel.setTitle(getString(R.string.search_text));
            drawerModel.setItemID(0);
            drawerModel.setDrawableInt(getDrawable(R.drawable.ic_search));
            drawerModels.add(drawerModel);

            drawerModel = new DrawerModel();
            drawerModel.setTitle(getString(R.string.settings_text));
            drawerModel.setItemID(1);
            drawerModel.setDrawableInt(getDrawable(R.drawable.ic_setting));
            drawerModels.add(drawerModel);

            drawerModel = new DrawerModel();
            drawerModel.setTitle(getString(R.string.notification_text));
            drawerModel.setItemID(8);
            drawerModel.setDrawableInt(getDrawable(R.drawable.ic_notification));
            drawerModels.add(drawerModel);

            drawerModel = new DrawerModel();
            drawerModel.setTitle(getString(R.string.ranking_text));
            drawerModel.setItemID(7);
            drawerModel.setDrawableInt(getDrawable(R.drawable.ic_rankings));
            drawerModels.add(drawerModel);


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return drawerModels;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        setCurrentTabFragment(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * set fragment according to the tab selected
     *
     * @param position tab selected position
     */
    public void setCurrentTabFragment(int position) {
        Fragment fragment = null;

        if (position == 0) {
            fragment = new MomentsFragment();


        } else if (position == 1)
            fragment = new NotificationsFragment();


        else if (position == 2)
            fragment = new MomentsFragment();

        replaceFragment(fragment);
    }

    private void implementListeners() {
        homeBinding.toolbarhome.homebutton.tabLayout.addOnTabSelectedListener(Home.this);
    }

}
