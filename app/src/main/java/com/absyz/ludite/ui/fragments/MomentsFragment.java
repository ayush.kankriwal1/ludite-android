package com.absyz.ludite.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.MomentsFragmentBinding;
import com.absyz.ludite.databinding.RankingFragmentBinding;

public class MomentsFragment extends Fragment{

    MomentsFragmentBinding momentsFragmentBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        momentsFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.moments_fragment, container, false);

        return momentsFragmentBinding.getRoot();
    }
}
