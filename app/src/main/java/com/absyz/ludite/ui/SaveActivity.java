package com.absyz.ludite.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.ProfileActivityBinding;
import com.absyz.ludite.databinding.SaveActivityBinding;

public class SaveActivity extends AppCompatActivity {

    /*activity dataBinding*/
    SaveActivityBinding SaveActivityBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SaveActivityBinding = DataBindingUtil.setContentView(SaveActivity.this, R.layout.save_activity);

        SaveActivityBinding.saveBtn.setOnClickListener(v -> {
            launchLoginScreen();
        });
    }

    private void launchLoginScreen() {
        startActivity(new Intent(SaveActivity.this, WelcomeActivity.class));
    }
}