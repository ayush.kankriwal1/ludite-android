package com.absyz.ludite.ui.base;

import android.graphics.drawable.Drawable;

public class DrawerModel {


    private int unReadCount;
    private String title;
    private Drawable drawableInt;
    private int itemID;

    public void setUnReadCount(int unReadCount) {
        this.unReadCount = unReadCount;
    }

    public int getUnReadCount() {
        return unReadCount;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public int getItemID() {
        return itemID;
    }

    public void setDrawableInt(Drawable drawableInt) {
        this.drawableInt = drawableInt;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getDrawableInt() {
        return drawableInt;
    }

    public String getTitle() {
        return title;
    }
}
