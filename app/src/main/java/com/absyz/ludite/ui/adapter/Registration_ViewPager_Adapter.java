package com.absyz.ludite.ui.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.absyz.ludite.ui.RegistrationEmail_Fragment;
import com.absyz.ludite.ui.RegistrationPhone_Fragment;

public class Registration_ViewPager_Adapter extends FragmentPagerAdapter {

    public Registration_ViewPager_Adapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new RegistrationPhone_Fragment();
        }
        else if (position == 1)
        {
            fragment = new RegistrationEmail_Fragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "Phone";
        }
        else if (position == 1)
        {
            title = "Email";
        }
        return title;
    }
}
