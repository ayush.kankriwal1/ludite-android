package com.absyz.ludite.ui.base;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.os.StrictMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.HomeActivityBinding;
import com.absyz.ludite.inject.ApplicationController;
import com.absyz.ludite.ui.LoginActivity;
import com.absyz.ludite.ui.fragments.HomeFragment;
import com.absyz.ludite.ui.fragments.MomentsFragment;
import com.absyz.ludite.ui.fragments.NotificationsFragment;
import com.absyz.ludite.ui.fragments.RankingFragments;
import com.absyz.ludite.ui.fragments.SearchFragment;
import com.absyz.ludite.ui.fragments.SettingsFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

public class HomeActivity extends  AppCompatActivity implements View.OnClickListener,
        NavigationView.OnNavigationItemSelectedListener, TabLayout.BaseOnTabSelectedListener {

    HomeActivityBinding homeActivityBinding;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (homeActivityBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                homeActivityBinding.drawerLayout.closeDrawers();
            } else {
             /*   if (ApplicationController.isRunning) {
                    new DialogUtil(mContext).alertDialogTwoButtons(this, this.getResources().getString(R.string.you_will_lose));
                } else {
                    super.onBackPressed();
                }*/
            }
            return true;
        }
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeActivityBinding = DataBindingUtil.setContentView(HomeActivity.this, R.layout.home_activity);
        initialize();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();//this is for taking pic from camera in device greater than 7.0
        StrictMode.setVmPolicy(builder.build());
//        setupTabIcons();
//        implementListeners();

    }

    private void implementListeners() {
        homeActivityBinding.homebutton.tabLayout.addOnTabSelectedListener(HomeActivity.this);
    }
    /**
     * method to add tabs items
     */
    private void setupTabIcons() {
        setCustomLayout(getResources().getDrawable(R.drawable.ic_memories));
        setCustomLayout(getResources().getDrawable(R.drawable.ic_friends));
        setCustomLayout(getResources().getDrawable(R.drawable.ic_group_friedns));
    }

    /**
     * method to set custom view for tabs items
     */
    private void setCustomLayout(Drawable drawable) {
        ConstraintLayout customTab = (ConstraintLayout) LayoutInflater.from(this)
                .inflate(R.layout.custom_item_tab_home, null);
        AppCompatImageView tv_title = customTab.findViewById(R.id.tv_icon);
        tv_title.setBackgroundDrawable(drawable);
        homeActivityBinding.homebutton.tabLayout.addTab(homeActivityBinding.homebutton.tabLayout.newTab().setCustomView(customTab));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initialize() {

        actionBarDrawerToggle = new ActionBarDrawerToggle(HomeActivity.this, homeActivityBinding.drawerLayout,
                homeActivityBinding.toolbar, 0, 0);

        homeActivityBinding.drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
//        homeActivityBinding.toolbar.setNavigationIcon(R.drawable.ic_hamburger_menu);
        actionBarDrawerToggle.syncState();

        View headerView = getLayoutInflater().inflate(R.layout.layout_header, homeActivityBinding.navView, false);
        homeActivityBinding.navView.addHeaderView(headerView);

        homeActivityBinding.navView.setNavigationItemSelectedListener(this);
        homeActivityBinding.drawerLayout.addDrawerListener(actionBarDrawerToggle);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Method to replace fragments
     *
     * @param fragment fragment to be replaced
     */
    public void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }


    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        homeActivityBinding.drawerLayout.closeDrawers();  // CLOSE DRAWER

        switch (item.getItemId()) {
            case R.id.menu_home:
                replaceFragment(new HomeFragment());
//                setToolbarTitle(getString(R.string.home_text));
                break;

            case R.id.menu_serch:
                replaceFragment(new SearchFragment());
//                setToolbarTitle(getString(R.string.timer_text));
                break;

            case R.id.menu_settings:
                replaceFragment(new SettingsFragment());
//                setToolbarTitle(getString(R.string.my_groups_text));
                break;
            case R.id.menu_notification:
                replaceFragment(new NotificationsFragment());
//                setToolbarTitle(getString(R.string.settings_text));
                break;
            case R.id.menu_ranking:
                replaceFragment(new RankingFragments());
//                setToolbarTitle(getString(R.string.invitations));
                break;

            default:
                break;
        }
        return false;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        setCurrentTabFragment(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    /**
     * set fragment according to the tab selected
     *
     * @param position tab selected position
     */
    public void setCurrentTabFragment(int position) {
        Fragment fragment = null;

        if (position == 0) {
            fragment = new MomentsFragment();
        } else if (position == 1)
            fragment = new NotificationsFragment();
        else if (position == 2)
            fragment = new MomentsFragment();

        replaceFragment(fragment);
    }




}
