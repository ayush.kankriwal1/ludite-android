package com.absyz.ludite.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.EmailRegistrationFragmentBinding;

import com.absyz.ludite.utils.ValidationUtils;

import java.util.Objects;

public class RegistrationEmail_Fragment extends Fragment {


    EmailRegistrationFragmentBinding emailRegistrationFragmentBinding ;
    private String strEmail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        emailRegistrationFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.email_registration_fragment, container, false);

        return emailRegistrationFragmentBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        emailRegistrationFragmentBinding.btNext.setAlpha(0.5f);
        emailRegistrationFragmentBinding.btNext.setClickable(false);
        emailRegistrationFragmentBinding.btNext.setEnabled(false);
        emailRegistrationFragmentBinding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                strEmail  = Objects.requireNonNull(emailRegistrationFragmentBinding.etEmail.getText()).toString();
                if (strEmail.isEmpty()){
                    emailRegistrationFragmentBinding.btNext.setAlpha(0.3f);
                    emailRegistrationFragmentBinding.btNext.setClickable(false);
                    emailRegistrationFragmentBinding.btNext.setEnabled(false);
                    emailRegistrationFragmentBinding.errorEmail.setText("");

                }else {
                    emailRegistrationFragmentBinding.btNext.setAlpha(1f);
                    emailRegistrationFragmentBinding.btNext.setClickable(true);
                    emailRegistrationFragmentBinding.btNext.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                emailRegistrationFragmentBinding.errorEmail.setVisibility(View.INVISIBLE);

            }
        });
        emailRegistrationFragmentBinding.btNext.setOnClickListener(v -> {
            emailValidation();

        });
    }

    private void emailValidation() {
        ValidationUtils.hideKeyboard(getActivity());

        boolean isValidEmail = true;
        if (TextUtils.isEmpty(emailRegistrationFragmentBinding.etEmail.getText().toString())) {
            emailRegistrationFragmentBinding.errorEmail.setVisibility(View.VISIBLE);
            emailRegistrationFragmentBinding.errorEmail.setText(getResources().getString(R.string.enter_email));
            isValidEmail = false;
        } else if (ValidationUtils.isValidEmailID(emailRegistrationFragmentBinding.etEmail.getText().toString())) {
            emailRegistrationFragmentBinding.errorEmail.setVisibility(View.VISIBLE);
            emailRegistrationFragmentBinding.errorEmail.setText(getResources().getText(R.string.valid_enter_email));
            isValidEmail = false;
        }
        if (isValidEmail) {
            launchNextScreen();
        }
    }

    private void launchNextScreen() {
        startActivity(new Intent(getContext(), ConfirmationCode_Activity.class));
    }



  
}