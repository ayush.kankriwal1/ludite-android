package com.absyz.ludite.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.AddNameActivityBinding;

import java.util.Objects;

public class AddName_Activity extends AppCompatActivity {

    /*activity dataBinding*/
    AddNameActivityBinding nameActivityBinding;
    private String strfullName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nameActivityBinding = DataBindingUtil.setContentView(AddName_Activity.this, R.layout.add_name_activity);

        nameActivityBinding.btNext.setAlpha(0.5f);
        nameActivityBinding.btNext.setClickable(false);
        nameActivityBinding.btNext.setEnabled(false);
        nameActivityBinding.etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                strfullName  = Objects.requireNonNull(nameActivityBinding.etName.getText()).toString();
                if (strfullName.isEmpty()){
                    nameActivityBinding.btNext.setAlpha(0.3f);
                    nameActivityBinding.btNext.setClickable(false);
                    nameActivityBinding.btNext.setEnabled(false);
                    nameActivityBinding.errorFullname.setText("");

                }else {
                    nameActivityBinding.btNext.setAlpha(1f);
                    nameActivityBinding.btNext.setClickable(true);
                    nameActivityBinding.btNext.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                nameActivityBinding.errorFullname.setVisibility(View.INVISIBLE);

            }
        });
        nameActivityBinding.btNext.setOnClickListener(v -> {
            nameValidation();
        });
    }

    private void nameValidation() {
        strfullName = nameActivityBinding.etName.getText().toString();

        if (strfullName.isEmpty()){
            nameActivityBinding.errorFullname.setVisibility(View.VISIBLE);
            nameActivityBinding.errorFullname.setText(getString(R.string.enter_full_name));
        }else if (strfullName.length()>15 && strfullName.length()<3){
            nameActivityBinding.errorFullname.setVisibility(View.VISIBLE);
            nameActivityBinding.errorFullname.setText(getString(R.string.enter_valid_full_name));
        }else {
            startActivity(new Intent(AddName_Activity.this, Password_Activity.class));
        }
    }
}
