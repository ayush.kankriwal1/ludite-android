package com.absyz.ludite.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.ProfileActivityBinding;
import com.absyz.ludite.databinding.WelcomeActivityBinding;

public class WelcomeActivity extends AppCompatActivity {

    /*activity dataBinding*/
    WelcomeActivityBinding WelcomeActivityBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WelcomeActivityBinding = DataBindingUtil.setContentView(WelcomeActivity.this, R.layout.welcome_activity);

        WelcomeActivityBinding.btNext.setOnClickListener(v -> {
            launchLoginScreen();
        });
    }

    private void launchLoginScreen() {
        startActivity(new Intent(WelcomeActivity.this, ContactsActivity.class));
    }
}