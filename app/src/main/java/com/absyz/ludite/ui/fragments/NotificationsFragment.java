package com.absyz.ludite.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.NotificationFragmentBinding;

public class NotificationsFragment extends Fragment {

    NotificationFragmentBinding notificationFragmentBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        notificationFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.notification_fragment, container, false);

        return notificationFragmentBinding.getRoot();
    }
}
