package com.absyz.ludite.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.HomeFragmentBinding;
import com.absyz.ludite.databinding.SearchFragmentBinding;

public class SearchFragment extends Fragment {

     SearchFragmentBinding searchFragmentBinding ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        searchFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.search_fragment, container, false);

        return searchFragmentBinding.getRoot();
    }
}
