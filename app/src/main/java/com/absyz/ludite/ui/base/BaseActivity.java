//package com.absyz.ludite.ui.base;
//
//
//import android.app.Dialog;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.res.Configuration;
//import android.net.ConnectivityManager;
//import android.net.NetworkInfo;
//import android.os.Build;
//import android.os.Bundle;
//import android.text.SpannableStringBuilder;
//import android.text.Spanned;
//import android.text.TextUtils;
//import android.text.style.ForegroundColorSpan;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.Window;
//import android.view.WindowManager;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AlertDialog;
//import androidx.appcompat.app.AppCompatActivity;
//
//import com.absyz.ludite.R;
////import com.google.android.material.bottomnavigation.BottomNavigationView;
////import com.jt.bayat.R;
////import com.jt.bayat.screens.dashboard.DashBoardActivity;
////import com.jt.bayat.screens.language_selection.model.GlobalLabels;
////import com.jt.bayat.screens.no_internet.NoInternetActivity;
////import com.jt.bayat.utils.AlertDialogUtils;
////import com.jt.bayat.utils.AppConstants;
////import com.jt.bayat.utils.RoomsDataSingleTon;
////import com.jt.bayat.utils.SessionStore;
////import com.jt.bayat.utils.Util;
////import com.jt.bayat.utils.Utility;
//
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.List;
//
//import javax.inject.Inject;
//
//public class BaseActivity extends AppCompatActivity {
//
//    private NetworkChangeReceiver mNetworkReceiver;
//    private Dialog dialog;
//    private ImageView iv;
//    private Animation anim;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        mNetworkReceiver = new NetworkChangeReceiver();
//        initLoader();
//    }
//
//    public void setStatusBarColor() {
//        setStatusBarColor(R.color.colorPrimary);
//    }
//    public void setStatusBarColor(int colorId) {
//        Window window =  getWindow();
//// clear FLAG_TRANSLUCENT_STATUS flag:
//        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
//        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//// finally change the color
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
////            window.setStatusBarColor(getResources().getColor(colorId));
//            window.setStatusBarColor(getResources().getColor(R.color.transparentFull));
//        }
//    }
//
//
//    private void initLoader() {
//        dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
////        dialog.setContentView(R.layout.image_view_logo);
////        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
////        iv = dialog.findViewById(R.id.iv);
////        anim = AnimationUtils.loadAnimation(this, R.anim.filp_image);
//
//    }
//
////    public GlobalLabels getGlobalLabels() {
////        return globalLabels;
////    }
////
////    public void setGlobalLabels(GlobalLabels globalLabels) {
////        this.globalLabels = globalLabels;
////    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        registerNetworkBroadcast();
//    }
//
//    private void registerNetworkBroadcast() {
//        try {
//            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        unregisterNetworkChanges();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
////        utility.hideKeyboard(this);
//    }
//
//    public void showToast(String toastMessage) {
//        if (!TextUtils.isEmpty(toastMessage)) {
//            LayoutInflater layoutInflater = getLayoutInflater();
//            View layout = layoutInflater.inflate(R.layout.item_toast, null);
//            TextView customTextView = layout.findViewById(R.id.tv_item_toast);
//            customTextView.setText(toastMessage);
//            //Creating the Toast object
//            Toast toast = new Toast(getApplicationContext());
//            toast.setDuration(Toast.LENGTH_SHORT);
//            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//            toast.setView(layout);//setting the view of custom toast layout
//            toast.show();
//        }
//
//    }
//
//    public boolean isNetworkConnected() {
//        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        if (cm.getActiveNetworkInfo() == null) {
//            startActivity(new Intent(this, NoInternetActivity.class));
//            return false;
//        } else return true;
//    }
//
//
//    protected void navigateToNoInternetScreen() {
//        try {
//            Utility.showNoConnectionDialog(getSupportFragmentManager(), getString(R.string.no_connection));
//            //Intent noInternet = new Intent(this, NoInternetActivity.class);
//            //startActivity(noInternet);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    protected void showAlert(String message) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        String selectTitle = getResources().getString(R.string.warning_txt);
//        // Initialize a new foreground color span instance
//        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(getResources().getColor(R.color.brown));
//
//        // Initialize a new spannable string builder instance
//        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(selectTitle);
//
//        // Apply the text color span
//        ssBuilder.setSpan(
//                foregroundColorSpan,
//                0,
//                selectTitle.length(),
//                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
//        );
//        builder.setMessage(message);
//        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        });
//
//        AlertDialog alertDialog = builder.create();
//        alertDialog.show();
//    }
//
//    public void showLoader() {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    if (dialog != null) {
//                        iv.setAnimation(anim);
//                        dialog.show();
//                    }
////                    utility.showDialog();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//    }
//
//    public void dismissLoader() {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    if (dialog != null)
//                        dialog.dismiss();
////                    utility.dismissDialog();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//    }
//    protected void showPopupIfSessionExpires() {
//        Long lastSignInTime = SessionStore.getSignInTime();
//        if(lastSignInTime != AppConstants.SIGN_IN_DEFAULT_TIME){
//            if((Calendar.getInstance().getTimeInMillis() - lastSignInTime) > AppConstants.SESSION_EXPIRE_TIME)
//                showTokeExpireAlert(null);
//        }
//    }
//
//    public void showTokeExpireAlert(String strmsg) {
//        AlertDialogUtils.showAlertToLogout(this,
//                null,
//                getString(R.string.token_expired),
//                0,
//                getString(R.string.ok),
//                null,
//                new AlertDialogUtils.OnButtonClickListeners() {
//                    @Override
//                    public void onPositiveButtonClick(DialogInterface dialog, int which) {
//                        clearUserLoginData();
//                        moveToHomeLandingPage();
//                    }
//
//                    @Override
//                    public void onNegativeButtonClick(DialogInterface dialog, int which) {
//
//                    }
//                }, false);
//
//
//    }
//
//    public void moveToHomeLandingPage() {
//        clearBookingDataLocally();
//        DashBoardActivity.gotoDashBoardActivity(this);
//    }
//
//    private void clearBookingDataLocally() {
//        RoomsDataSingleTon.getInstance().clearRoomData(false);
//        Util.hotelData.clearData();
//        SessionStore.clearHotelData(this);
//    }
//
//    private void clearUserLoginData() {
//        SessionStore.setSignInTime(AppConstants.SIGN_IN_DEFAULT_TIME);
//        SessionStore.storeString(this, SessionStore.AUTHORIZATION, "");
//        new SessionStore(this).storeLoginData(null);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//    }
//
//    public void showSimpleAlert(String strMsg) {
//        if (!TextUtils.isEmpty(strMsg))
//            AlertDialogUtils.showAlert(this, strMsg, null, getString(R.string.cancel), null, false);
//    }
//
//    public void showErrorBasedOnLanguage(Error error) {
//
//        try {
//            if (SessionStore.getLanguage(this).equalsIgnoreCase(SessionStore.arabic)) {
//                showToast(error.getMessage_ar());
//            } else
//                showToast(error.getMessage());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    protected void unregisterNetworkChanges() {
//        try {
//            unregisterReceiver(mNetworkReceiver);
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private boolean isOnline(Context context) {
//        try {
//            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo netInfo = cm.getActiveNetworkInfo();
//            //should check null because in airplane mode it will be null
//            return (netInfo != null && netInfo.isConnected());
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//            return false;
//        }
//    }
//
//    protected void showNetworkActivity() {
//        Intent intent = new Intent(this, NoInternetActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        startActivity(intent);
//    }
//
//    public class NetworkChangeReceiver extends BroadcastReceiver {
//
//        private String TAG = getClass().getSimpleName();
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            Log.e(TAG, "Conectivity Failure !!! ");
//            try {
//                if (!isOnline(context)) {
//                    showNetworkActivity();
//                }
//
//            } catch (NullPointerException e) {
//                e.printStackTrace();
//            }
//        }
//
//    }
//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        Log.e("BaseActivity", "onConfigurationChanged: "  );
//    }
//    public void setNavigationMenu(Context context, BottomNavigationView navView) {
//        try {
//            navView.setVisibility(View.VISIBLE);
//            Utility.setFontNavigations(this, navView);
//            navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//                @Override
//                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
//                    Util.hotelData.clearData();
//                    menuItem.setChecked(true);
//                    Menu menu = navView.getMenu();
//                    List<MenuItem> items = new ArrayList<>();
//                    for(int i = 0; i < menu.size(); i++){
//                        items.add(menu.getItem(i));
//                    }
//                    int position = items.indexOf(menuItem);
//                    DashBoardActivity.gotoDashBoardActivity(context, position);
//                    return true;
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//    public void setMenuItem(BottomNavigationView navView, int position) {
//        navView.getMenu().getItem(position).setChecked(true);
//    }
//}
