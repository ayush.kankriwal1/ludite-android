package com.absyz.ludite.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.RegistrationActivityBinding;
import com.absyz.ludite.ui.adapter.Registration_ViewPager_Adapter;
import com.google.android.material.tabs.TabLayout;

public class RegistrationActivity extends AppCompatActivity {

    /*activity dataBinding*/
    RegistrationActivityBinding registrationActivityBinding;

    Registration_ViewPager_Adapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registrationActivityBinding = DataBindingUtil.setContentView(RegistrationActivity.this, R.layout.registration_activity);

        viewPagerAdapter = new Registration_ViewPager_Adapter(getSupportFragmentManager());
        registrationActivityBinding.regViewpager.setAdapter(viewPagerAdapter);
        registrationActivityBinding.regTab.setupWithViewPager(registrationActivityBinding.regViewpager);

    }

}
