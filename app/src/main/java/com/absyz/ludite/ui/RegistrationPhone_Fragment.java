package com.absyz.ludite.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.PhoneRegistrationFragmentBinding;
import com.absyz.ludite.utils.ValidationUtils;
import java.util.Objects;


public class RegistrationPhone_Fragment  extends Fragment {


    PhoneRegistrationFragmentBinding phoneRegistrationFragmentBinding ;
    private  String strPhone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        phoneRegistrationFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.phone_registration_fragment, container, false);

        return phoneRegistrationFragmentBinding.getRoot();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        phoneRegistrationFragmentBinding.btNext.setAlpha(0.5f);
        phoneRegistrationFragmentBinding.btNext.setClickable(false);
        phoneRegistrationFragmentBinding.btNext.setEnabled(false);
        phoneRegistrationFragmentBinding.etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                strPhone  = Objects.requireNonNull(phoneRegistrationFragmentBinding.etPhone.getText()).toString();
                if (strPhone.isEmpty()){
                    phoneRegistrationFragmentBinding.btNext.setAlpha(0.3f);
                    phoneRegistrationFragmentBinding.btNext.setClickable(false);
                    phoneRegistrationFragmentBinding.btNext.setEnabled(false);
                    phoneRegistrationFragmentBinding.errorPhone.setText("");

                }else {
                    phoneRegistrationFragmentBinding.btNext.setAlpha(1f);
                    phoneRegistrationFragmentBinding.btNext.setClickable(true);
                    phoneRegistrationFragmentBinding.btNext.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                phoneRegistrationFragmentBinding.errorPhone.setVisibility(View.INVISIBLE);

            }
        });
        phoneRegistrationFragmentBinding.btNext.setOnClickListener(v -> {
            phoneValidation();

        });
    }

    private void phoneValidation() {
        ValidationUtils.hideKeyboard(getActivity());

        boolean isValidMobileNo = true;
        if (TextUtils.isEmpty(phoneRegistrationFragmentBinding.etPhone.getText().toString())) {
            phoneRegistrationFragmentBinding.errorPhone.setVisibility(View.VISIBLE);
            phoneRegistrationFragmentBinding.errorPhone.setText(getResources().getString(R.string.enter_phone));
            isValidMobileNo = false;
        } else if (!ValidationUtils.isValidMobile(phoneRegistrationFragmentBinding.etPhone.getText().toString())) {
            phoneRegistrationFragmentBinding.errorPhone.setVisibility(View.VISIBLE);
            phoneRegistrationFragmentBinding.errorPhone.setText(getResources().getText(R.string.valid_enter_phone));
            isValidMobileNo = false;
        }
        if (isValidMobileNo) {
            launchNextScreen();
        }
    }

    private void launchNextScreen() {
        startActivity(new Intent(getContext(), ConfirmationCode_Activity.class));
    }

}