package com.absyz.ludite.ui;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.DatePicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.AddDobActivityBinding;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Objects;


public class AddDateofBirth_Activity extends AppCompatActivity {

    AddDobActivityBinding DobActivityBinding;
    String date_val;
    private String strDate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DobActivityBinding = DataBindingUtil.setContentView(AddDateofBirth_Activity.this, R.layout.add_dob_activity);


        DobActivityBinding.btNext.setAlpha(0.5f);
        DobActivityBinding.btNext.setClickable(false);
        DobActivityBinding.btNext.setEnabled(false);
        DobActivityBinding.etDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                strDate  = Objects.requireNonNull(DobActivityBinding.etDate.getText()).toString();
                if (strDate.isEmpty()){
                    DobActivityBinding.btNext.setAlpha(0.3f);
                    DobActivityBinding.btNext.setClickable(false);
                    DobActivityBinding.btNext.setEnabled(false);
                    DobActivityBinding.errorDob.setText("");

                }else {
                    DobActivityBinding.btNext.setAlpha(1f);
                    DobActivityBinding.btNext.setClickable(true);
                    DobActivityBinding.btNext.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                DobActivityBinding.errorDob.setVisibility(View.INVISIBLE);

            }
        });



        int year, month, dayOfMonth;
                Calendar calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddDateofBirth_Activity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                if(month<10)
                                    date_val = year+"-0"+(month + 1);
                                else
                                    date_val = year+"-"+(month + 1);
                                if(day<10)
                                    date_val+="-0"+day;
                                else
                                    date_val+="-"+day;

                                DobActivityBinding.etDate.setText(date_val);
                                //start_date.setText(start_Date_val);
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();

        DobActivityBinding.datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int year, month, dayOfMonth;
                Calendar calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddDateofBirth_Activity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                if(month<10)
                                    date_val = year+"-0"+(month + 1);
                                else
                                    date_val = year+"-"+(month + 1);
                                if(day<10)
                                    date_val+="-0"+day;
                                else
                                    date_val+="-"+day;

                                DobActivityBinding.etDate.setText(date_val);
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        DobActivityBinding.btNext.setOnClickListener(v -> {
            datevalidation();
        });
    }

    private void datevalidation() {
        strDate = DobActivityBinding.etDate.getText().toString();

        if (strDate.isEmpty()){
            DobActivityBinding.errorDob.setVisibility(View.VISIBLE);
            DobActivityBinding.errorDob.setText(getString(R.string.enter_code));
        }else {
            startActivity(new Intent(AddDateofBirth_Activity.this, WelcomeActivity.class));
        }
    }


}
