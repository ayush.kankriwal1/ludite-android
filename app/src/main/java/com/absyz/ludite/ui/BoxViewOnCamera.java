package com.absyz.ludite.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.view.View;

public class BoxViewOnCamera extends View {
    public BoxViewOnCamera(Context context) {
        super(context);
    }

    @SuppressLint("WrongConstant")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);        canvas.save();

        final Rect rect = new Rect(40, getHeight()-(int)(getHeight()/1.2), getWidth()-40, getHeight()-(getHeight()/6));
        final RectF rectF = new RectF(rect);
        Path path = new Path();
        path.addRoundRect(rectF, (float)50, (float)50, Path.Direction.CCW);
        canvas.clipPath(path, Region.Op.DIFFERENCE);



    /*canvas.clipRect(50, 50, getWidth()-50, getHeight()-50,
            Region.Op.DIFFERENCE);*/

        canvas.drawColor(Color.parseColor("#59000000"));
        canvas.restore();
    }
}
