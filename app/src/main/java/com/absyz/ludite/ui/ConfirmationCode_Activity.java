package com.absyz.ludite.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.ConfirmationCodeActivityBinding;

import java.util.Objects;

public class ConfirmationCode_Activity extends AppCompatActivity {

    /*activity dataBinding*/
    ConfirmationCodeActivityBinding CodeActivityBinding;
    private String strConfirmCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CodeActivityBinding = DataBindingUtil.setContentView(ConfirmationCode_Activity.this, R.layout.confirmation_code_activity);

        CodeActivityBinding.btNext.setAlpha(0.5f);
        CodeActivityBinding.btNext.setClickable(false);
        CodeActivityBinding.btNext.setEnabled(false);
        CodeActivityBinding.etConfirmcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                strConfirmCode  = Objects.requireNonNull(CodeActivityBinding.etConfirmcode.getText()).toString();
                if (strConfirmCode.isEmpty()){
                    CodeActivityBinding.btNext.setAlpha(0.3f);
                    CodeActivityBinding.btNext.setClickable(false);
                    CodeActivityBinding.btNext.setEnabled(false);
                    CodeActivityBinding.errorConfirmcode.setText("");

                }else {
                    CodeActivityBinding.btNext.setAlpha(1f);
                    CodeActivityBinding.btNext.setClickable(true);
                    CodeActivityBinding.btNext.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                CodeActivityBinding.errorConfirmcode.setVisibility(View.INVISIBLE);

            }
        });
        CodeActivityBinding.btNext.setOnClickListener(v -> {
            confirmCodValidation();
        });

        CodeActivityBinding.tvResend.setOnClickListener(v -> {
            Toast.makeText(this,"Code has been sent to you Mobile!", Toast.LENGTH_SHORT).show();
        });
    }
    private void confirmCodValidation() {
        strConfirmCode = CodeActivityBinding.etConfirmcode.getText().toString();

        if (strConfirmCode.isEmpty()){
            CodeActivityBinding.errorConfirmcode.setVisibility(View.VISIBLE);
            CodeActivityBinding.errorConfirmcode.setText(getString(R.string.enter_code));
        }else if (strConfirmCode.length()!=6){
            CodeActivityBinding.errorConfirmcode.setVisibility(View.VISIBLE);
            CodeActivityBinding.errorConfirmcode.setText(getString(R.string.enter_valid_code));
        }else {
            startActivity(new Intent(ConfirmationCode_Activity.this, AddName_Activity.class));
        }
    }

}