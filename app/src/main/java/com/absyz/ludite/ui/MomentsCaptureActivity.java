package com.absyz.ludite.ui;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.MomentsActivityBinding;
import com.absyz.ludite.ui.base.Home;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MomentsCaptureActivity extends AppCompatActivity implements SurfaceHolder.Callback, View.OnClickListener{
    private static final int REQUEST_PERMISSION_REQ_CODE = 908;
    private static final int CROPING_CODE = 3;
    MomentsActivityBinding momentsActivityBinding;
    private Camera camera;
    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private  Camera.PictureCallback jpegCallback;
   private BoxViewOnCamera boxView;
    private List<String> listPermissionsNeeded = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        momentsActivityBinding = DataBindingUtil.setContentView(MomentsCaptureActivity.this, R.layout.moments_activity);
        StrictMode.ThreadPolicy old = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(old)
                .permitDiskWrites()
                .build());
        StrictMode.setThreadPolicy(old);

        if (ActivityCompat.checkSelfPermission(MomentsCaptureActivity.this, "android.permission.CAMERA") != PackageManager.PERMISSION_GRANTED) {

            listPermissionsNeeded.add("android.permission.CAMERA");
        }
        if (ActivityCompat.checkSelfPermission(MomentsCaptureActivity.this, "android.permission.READ_EXTERNAL_STORAGE")
                != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add("android.permission.READ_EXTERNAL_STORAGE");
        }
        if (ActivityCompat.checkSelfPermission(MomentsCaptureActivity.this, "android.permission.WRITE_EXTERNAL_STORAGE")
                != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add("android.permission.WRITE_EXTERNAL_STORAGE");
        }

        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_PERMISSION_REQ_CODE);
        } else {
            init();
//            activityPause = true;
        }
    }

    private void init() {
        surfaceHolder =   momentsActivityBinding.previewView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        boxView = new BoxViewOnCamera(MomentsCaptureActivity.this);

        jpegCallback = (data, camera) -> {
//            runOnUiThread(() -> new SaveImageTask().execute(data));
        };
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (camera != null) {
            surfaceHolder.removeCallback(this);
            camera.stopPreview();
            camera.stopFaceDetection();
            camera.release();
            camera = null;
        }
    }
    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        camera.stopPreview();
        camera.release();
        camera = null;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, final @NonNull String[] permissions,
                                           final @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_REQ_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    init();
//                    activityPause = true;
                    camera_open();

                }
                break;
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 0, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        if (!TextUtils.isEmpty(path))
            return Uri.parse(path);
        else
            return Uri.parse("");
    }


    private Bitmap rotateImage(Bitmap src, float degree) {
        // create new matrix
        Matrix matrix = new Matrix();
        // setup rotation degree
        matrix.postRotate(degree);
        Bitmap bmp = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
        return bmp;
    }

    public void camera_open() {
        if (camera != null) {
            surfaceHolder.removeCallback(this);
            surfaceHolder.addCallback(null);
            camera.stopPreview();
            camera.stopFaceDetection();
            camera.release();
            camera = null;
        }
        try {
            surfaceView = momentsActivityBinding.previewView;
            surfaceHolder = momentsActivityBinding.previewView.getHolder();
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
            // open the camera
            camera = Camera.open();
        } catch (RuntimeException e) {
            // check for exceptions
            System.err.println(e);

            return;
        }
        try {
//        setCameraDisplayOrientation(this, 0, camera);
            camera.setDisplayOrientation(90);
//        surfaceView.setCameraDistance(180);
            Camera.Parameters param;
            param = camera.getParameters();
            int screenWidth = (int) getResources().getDisplayMetrics().widthPixels;
            int screenHeight = (int) getResources().getDisplayMetrics().heightPixels;
            List<Camera.Size> sizes = param.getSupportedPreviewSizes();
            Camera.Size optimalSize = getOptimalPreviewSize(sizes, screenWidth, screenHeight);

            param.setPreviewSize(optimalSize.width, optimalSize.height);
     /*   Camera.Size myBestSize = getBestPreviewSize(width, height, param);
        // modify parameter
        param.setPreviewSize(myBestSize.width, myBestSize.height);*/
            param.setPictureFormat(ImageFormat.JPEG);
            param.setJpegQuality(100);
            List<String> focusModes = param.getSupportedFocusModes();
            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                param.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            } else if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                param.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            }
            param.setPictureSize(optimalSize.width, optimalSize.height);
            camera.setParameters(param);

            // The Surface has been created, now tell the camera where to draw
            // the preview.
            camera.stopPreview();
            camera.startPreview();
            camera.setPreviewDisplay(surfaceHolder);
//            frameLayout.addView(boxView);
        } catch (Exception e) {
            // check for exceptions
            System.err.println(e);
            Toast.makeText(this, "Please Check Camera", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double) h / w;

        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.height / size.width;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;

            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }

        return optimalSize;
    }


}
