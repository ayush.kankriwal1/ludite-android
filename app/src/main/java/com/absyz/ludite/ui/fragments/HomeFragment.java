package com.absyz.ludite.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.HomeFragmentBinding;
import com.absyz.ludite.ui.SplashActivity;

public class HomeFragment extends Fragment {

    HomeFragmentBinding homeFragmentBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        homeFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false);

        return homeFragmentBinding.getRoot();
    }

}