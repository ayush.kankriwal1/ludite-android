package com.absyz.ludite.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.ContactsActivityBinding;
import com.absyz.ludite.databinding.ProfileActivityBinding;

public class ContactsActivity extends AppCompatActivity {

    /*activity dataBinding*/
    ContactsActivityBinding ContactsActivityBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ContactsActivityBinding = DataBindingUtil.setContentView(ContactsActivity.this, R.layout.contacts_activity);

        ContactsActivityBinding.tvSkip.setOnClickListener(v -> {
            launchLoginScreen();
        });
    }

    private void launchLoginScreen() {
        startActivity(new Intent(ContactsActivity.this, Profile_Activity.class));
    }
}