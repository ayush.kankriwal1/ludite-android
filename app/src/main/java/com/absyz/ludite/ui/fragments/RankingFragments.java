package com.absyz.ludite.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.NotificationFragmentBinding;
import com.absyz.ludite.databinding.RankingFragmentBinding;

public class RankingFragments extends Fragment {

    RankingFragmentBinding rankingFragmentBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rankingFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.ranking_fragment, container, false);

        return rankingFragmentBinding.getRoot();
    }
}
