package com.absyz.ludite.ui;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.Observable;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.LoginActivityBinding;
import com.absyz.ludite.inject.factory.ViewModelFactory;
import com.absyz.ludite.inject.helper.CustomTypefaceUtil;
import com.absyz.ludite.inject.helper.FontCache;
import com.absyz.ludite.inject.helper.constant;
import com.absyz.ludite.inject.viewmodel.LoginViewModel;
import com.absyz.ludite.ui.base.Home;
import com.absyz.ludite.ui.base.HomeActivity;

import javax.inject.Inject;

import static com.absyz.ludite.inject.ApplicationController.getContext;

/**
 * Created by Ayush on 26-05-2020.
 */
public class LoginActivity extends AppCompatActivity {

    /*activity dataBinding*/
    LoginActivityBinding loginActivityBinding;

    @Inject
    ViewModelFactory viewModelFactory;

    @Inject
    LoginViewModel loginViewModel;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginActivityBinding = DataBindingUtil.setContentView(LoginActivity.this, R.layout.login_activity);
        decorateText();

        loginActivityBinding.btSignIn.setOnClickListener(v -> {

            startActivity(new Intent(this, Home.class));
        });



    }

    /**
     * method to change text color and text font
     */
    private void decorateText() {

        ClickableSpan register = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                startActivity(new Intent(LoginActivity.this, LoginActivity.class));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        SpannableString ss = new SpannableString(getString(R.string.don_t_have_an_account_sign_up_text));
        ss.setSpan(register, 22, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.button_color_blue)),
                22, 30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        Typeface semiBold = ResourcesCompat.getFont(this, R.font.robotomedium);
        ss.setSpan(new CustomTypefaceUtil("", semiBold), 22, 30, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        loginActivityBinding.tvSignUp.setText(ss);
        loginActivityBinding.tvSignUp.setMovementMethod(LinkMovementMethod.getInstance());
        loginActivityBinding.tvSignUp.setHighlightColor(Color.TRANSPARENT);
        loginActivityBinding.tvSignUp.setOnClickListener(v -> {
            launchLoginScreen();
        });
    }

    private void launchLoginScreen() {
        startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
    }

}
