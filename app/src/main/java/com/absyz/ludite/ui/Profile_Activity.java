package com.absyz.ludite.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.absyz.ludite.R;
import com.absyz.ludite.databinding.ProfileActivityBinding;

public class Profile_Activity extends AppCompatActivity {

    /*activity dataBinding*/
    ProfileActivityBinding ProfileActivityBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ProfileActivityBinding = DataBindingUtil.setContentView(Profile_Activity.this, R.layout.profile_activity);

        ProfileActivityBinding.tvSkip.setOnClickListener(v -> {
            launchLoginScreen();
        });
    }

    private void launchLoginScreen() {
        startActivity(new Intent(Profile_Activity.this, SaveActivity.class));
    }
}